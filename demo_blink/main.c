// main.c
// Blink demo for TI TM4C123GXL, using Keil v5
// Program flashes red/green at 1Hz

// This file is part of Blink Demo
// Travis Llado, travis@travisllado.com
// Last modified 2020-04-06

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include "PLL.h"
#include "lladoware_output_pins.h"
#include "lladoware_timing.h"

////////////////////////////////////////////////////////////////////////////////
// int main()

int main(void) {
    // Initialize all hardware
    PLL_Init();
    onboard_leds_init();
    // All other functions performed by interrupt handlers

    // Spin forever
    while(1) {
        onboard_leds_green_update(LOW);
        onboard_leds_red_update(HIGH);
        wait_ms(500);
        onboard_leds_red_update(LOW);
        onboard_leds_green_update(HIGH);
        wait_ms(500);
    }
}

////////////////////////////////////////////////////////////////////////////////
// End of file
